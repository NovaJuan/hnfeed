# Hacker News Nodejs' Feed

Check the latest posts about Node.js from Hacker News!

---

### Environment
- Node.js  14.16.0
- React.js 17.0.2
- Docker 20.10.5
 
---

### Requirements
- Docker
- ports `5000` and `3000` free

---

### Instructions to run
1. **Clone** the repo:
```
git clone https://gitlab.com/NovaJuan/hnfeed.git
```

2. **Change directory** to the project:
```
cd hnfeed/
```

3. Run **docker-compose**:
```
docker-compose up
```

4. **Navigate** to `http://localhost:3000`