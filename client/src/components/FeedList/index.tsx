import { useState,useEffect } from "react";
import * as React from 'react';
import {Container} from './styles';
import api from '../../lib/api';
import FeedListItem from './FeedListItem';
import INew from '../../interfaces/new.interface';

export default function FeedList(){
    const [posts,setPosts] = useState<INew[]>(null);

    useEffect(() => {
        (async () => {
            const res = await api.get('/news');
            const {data} = res.data;
            setPosts(data);
        })()
    },[])

    async function remove(id:string){
        const res = await api.delete(`/news/${id}`);

        if(res.data.success){
            setPosts(prev => {
                return prev.filter(item => item._id !== id)
            });
        }
    }

    return (
        <Container>
            {posts !== null && posts.map((post:INew) => (
                <FeedListItem post={post} key={post._id} remove={remove} />
            ))}
        </Container>
    )
}