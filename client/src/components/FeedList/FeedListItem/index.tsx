import * as React from 'react';
import {Container} from './styles';
import INew from '../../../interfaces/new.interface';
import {BsTrash} from 'react-icons/bs';
import * as moment from 'moment';


interface Props {
    post: INew,
    remove:Function
}

moment.updateLocale('en', {
    calendar : {
        lastDay : '[Yesterday]',
        sameDay : 'hh:mm a',
        nextDay : '[Tomorrow]',
        lastWeek : 'MMM DD',
        nextWeek : 'MMM DD',
        sameElse : 'L'
    }
});

export default function FeedListItem({post,remove}:Props){
    if(post.title === 'No Title'){
        return (<></>)
    }

    function goToPage(e:React.MouseEvent<HTMLElement>){
        if(post.url){
           document.getElementById(`url_${post._id}`).click();
        }
    }

    function discard(e:React.MouseEvent<HTMLElement>){
        e.stopPropagation();
        remove(post._id)
    }

    return (
        <Container onClick={goToPage} className='post'>
            {post.url && (
            <a href={post.url} target='_blank' id={`url_${post._id}`}></a>
            )}
            <p className='title'>{post.title} <span className="author">- {post.author} -</span> {!post.url && (<span className="error">No URL :(</span>)}</p>
            <span className="date">{moment(post.posted_date).calendar()}</span>
            <button className="discard" onClick={discard}><BsTrash /></button>
        </Container>
    )
}