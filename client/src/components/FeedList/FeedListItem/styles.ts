import styled from 'styled-components';

export const Container = styled.li`
    padding:15px 30px;
    cursor:pointer;
    font-size:13px;
    display:flex;
    align-items:center;
    font-weight:600;
    color:#333;

    &:not(:last-child){
        border-bottom: 1px solid #ccc;
    }

    background-color:#fff;
    
    &:hover{
        background-color:#fafafa;
    }

    .title{
        width:100%;
        color:#333;
    }

    .author{
        color:#999;
    }

    span{
        white-space:nowrap;
    }

    .error{
        color:red;
    }

    .date{
        margin-right:40px;
    }

    .discard{
        background:none;
        border:none;
        font-size:20px;
        margin-bottom:-7px;
    }
`;