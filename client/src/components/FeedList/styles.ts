import styled from 'styled-components';

export const Container = styled.ul`
    list-style:none;
    width:100%;
    margin: 10px 0;
`;