import styled from 'styled-components';

export const Container = styled.div`
    header{
        background-color:#333333;
        color:#fff;
        padding:40px 0;

        h1{
            font-size: 60px;
        }

        p{
            font-size: 20px;
        }
    }
`;