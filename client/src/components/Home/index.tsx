import * as React from 'react';
import {Container} from './styles';
import FeedList from '../FeedList';

export default function Home(){
    return(
        <Container>
            <header>
                <div className="container">
                <h1>HN Feed</h1>
                <p>We &lt;3 Hacker News</p>
                </div>
            </header>

            <div className="container">
            <FeedList />
            </div>
        </Container>
    )
}