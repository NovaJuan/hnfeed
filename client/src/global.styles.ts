import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`
  *{
      margin:0;
      padding:0;
      box-sizing: border-box;
      font-family: 'Balsamiq Sans', cursive;
  }

  .container{
    max-width:1200px;
    padding: 0 20px;
    margin:0 auto;
  }
`;
