import * as React from 'react';
import Home from './components/Home';
import GlobalStyles from './global.styles';

export default function App(){
    return (
        <>
        <GlobalStyles />
        <Home />
        </>
    )
}