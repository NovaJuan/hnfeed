export default interface INew {
    _id:string,
    title:string,
    author:string,
    url:string,
    posted_date:number,
    story_id:number
}