import axios from "axios";

const baseURL =
  process.env.NODE_ENV === "production"
    ? `${window.location.protocol}//${window.location.hostname}:5000`
    : "http://localhost:5000";

export default axios.create({
  baseURL,
  headers: { "Content-Type": "application/json" },
});
