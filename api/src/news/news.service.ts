import { Injectable } from '@nestjs/common';
import INew from './interfaces/new';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { NewDocument } from './schemas/new.schema';
import { Cron, CronExpression } from '@nestjs/schedule';
import axios from 'axios';
import { exists } from 'node:fs';

@Injectable()
export class NewsService {
  constructor(@InjectModel('New') private readonly model: Model<NewDocument>) {}

  async findAll(): Promise<INew[]> {
    return await this.model.find({ show: true }).sort({ posted_date: -1 });
  }

  async hide(id: string): Promise<INew> {
    return await this.model.findByIdAndUpdate(
      id,
      { show: false },
      { new: true },
    );
  }

  // @Cron(CronExpression.EVERY_MINUTE)
  @Cron(CronExpression.EVERY_HOUR)
  async fetchNews() {
    const res = await axios.get(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );
    let { hits: posts } = res.data;

    console.log(posts.length);

    for (let i = 0; i < posts.length; i++) {
      const post = posts[i];
      try {
        await this.model.create({
          title: post.story_title || post.title || 'No Title',
          author: post.author || 'No Author',
          url: post.story_url || post.url || null,
          posted_date: new Date(post.created_at).getTime(),
          story_id: post.objectID,
        });
      } catch (err) {
        if (err.code === 11000) {
          continue;
        }
        throw err;
      }
    }
  }
}
