export default interface INew{
    id?:string,
    title:string,
    url:string,
    author:string,
    posted_date:number,
    story_id:number,
    show:boolean
}