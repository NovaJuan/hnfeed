export default interface IResponse {
    success:boolean;
    data:any;
    error?:any;
}