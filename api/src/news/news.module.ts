import { Module } from '@nestjs/common';
import { NewsController } from './news.controller';
import {MongooseModule} from '@nestjs/mongoose'
import { NewsService } from './news.service';
import {NewSchema} from './schemas/new.schema';

@Module({
  imports: [MongooseModule.forFeature([{name:'New',schema:NewSchema}])],
  controllers: [NewsController],
  providers: [NewsService],
})
export class NewsModule {}
