import {Schema,Document} from 'mongoose';

export const NewSchema = new Schema({
    title:String,
    author:String,
    url:String,
    posted_date:Number,
    story_id:{
        unique:true,
        type:Number
    },
    show:{
        type:Boolean,
        default:true
    }
});

export interface NewDocument extends Document {
    id?:string
    title:string,
    author:string,
    url:string,
    posted_date:number,
    story_id:number,
    show:boolean
}