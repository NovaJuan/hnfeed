import { Controller,Get,Delete,Param } from '@nestjs/common';
import IResponse from './interfaces/response';
import {NewsService} from './news.service';


@Controller('news')
export class NewsController {
    constructor(private readonly service:NewsService){}

    @Get()
    async findAll():Promise<IResponse>{
       return {
            success:true,
            data:await this.service.findAll()
       } 
    }
    
    @Delete(':id')
    async delete(@Param('id') id):Promise<IResponse>{
        return {
             success:true,
             data:await this.service.hide(id)
        } 
    }
}
