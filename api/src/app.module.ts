import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {MongooseModule} from '@nestjs/mongoose'
import {MONGO_URI} from './config/keys'
import {NewsModule} from './news/news.module';
import { ScheduleModule } from '@nestjs/schedule';


@Module({
  imports: [MongooseModule.forRoot(MONGO_URI,{useCreateIndex:true,useFindAndModify:false}),NewsModule,ScheduleModule.forRoot()],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
